package com.devcamp.shape_api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shape_api.model.Circle;
import com.devcamp.shape_api.model.Rectangle;
import com.devcamp.shape_api.model.Square;

@RestController
public class ShapeControl {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea(){
        double radius = 6.5;
        Circle circle = new Circle(radius);
        return circle.getArea();
    }

    @GetMapping("/circle-perimeter")
    public double getCirclePerimeter(){
        double radius = 5.6;
        Circle circle = new Circle(radius);
        return circle.getPerimeter();
    }

    @GetMapping("/rectangle-area")
    public double getRectangleArea(){
        double width = 8.0;
        double height = 7.5;
        Rectangle rectangle = new Rectangle(width, height);
        return rectangle.getArea();
    }

    @GetMapping("/rectangle-perimeter")
    public double getRectanglePerimeter(){
        double width = 2.5;
        double height = 4.0;
        Rectangle rectangle = new Rectangle(width, height);
        return rectangle.getPerimeter();
    }

    @GetMapping("/square-area")
    public double getSquareArea(){
        double side = 6.0;
        Square square = new Square(side);
        return square.getArea();
    }

    @GetMapping("/square-perimeter")
    public double getSquarePerimeter(){
        double side = 5.0;
        Square square = new Square(side);
        return square.getPerimeter();
    }
}
