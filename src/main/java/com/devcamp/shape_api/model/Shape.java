package com.devcamp.shape_api.model;

public class Shape {
    String color = "red";
    boolean filled = true;

    public Shape(){
    }

    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public boolean isFilled(){
        return filled;
    }
    @Override
    public String toString(){
        return "'Shape[color = " + this.color + ", filled = " + this.filled + "]'";
    }
}
