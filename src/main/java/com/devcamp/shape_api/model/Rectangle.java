package com.devcamp.shape_api.model;

public class Rectangle extends Shape {
    double width = 1.0;
    double length = 1.0;

    public Rectangle(){
        super();
    }

    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }
    public Rectangle(double width, double length, String color, boolean filled){
        this.color = color;
        this.filled = filled;
        this.width = width;
        this.length = length;
    }
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getLength() {
        return length;
    }
    public void setLength(double length) {
        this.length = length;
    }
    public double getArea(){ //diện tích
        return width * length;
    }
    public double getPerimeter(){ // chu vi
        return 2 * (width + length);
    }
    @Override
    public String toString() {
        return "Rectangle [Shape [color= " + this.color + ", filled="
        + this.filled + "], width=" + width + ", length= " + length + "]";
    }

}
