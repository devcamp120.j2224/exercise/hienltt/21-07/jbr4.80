package com.devcamp.shape_api.model;

public class Circle extends Shape{
    double radius = 1.0;

    public Circle(){
        super();
    }

    public Circle(double radius){
        super();
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled){
        this.radius = radius;
        this.color = color;
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){ //diện tích
        return radius * radius * Math.PI;
    }

    public double getPerimeter(){ // chu vi
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return "Circle [ Shape [color= " + this.color + ", filled="
        + this.filled + "], radius=" + radius + "]";
    }
}
