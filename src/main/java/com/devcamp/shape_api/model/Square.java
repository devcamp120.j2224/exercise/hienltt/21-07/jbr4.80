package com.devcamp.shape_api.model;

public class Square extends Rectangle {
    
    public Square(){
    }

    public Square(double side){ 
        this.width = side;
        this.length = side;
    } 

    public Square(double side, String color, boolean filled){
        this.width = side;
        this.length = side;     
        this.color = color;
        this.filled = filled;  
    }

    public double getSide(){
        return width;
    }

    public double setSide(double side){
        width = side;
        return width;
    }

    @Override
    public String toString() {
        return "Square [Rectangle [Shape [color= " + this.color + ", filled="
        + this.filled + "], width=" + width + ", length= " + length + "]]";
    }
}
